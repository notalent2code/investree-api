package com.investree.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InvestreeApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(InvestreeApiApplication.class, args);
	}

}
