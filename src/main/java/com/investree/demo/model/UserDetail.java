package com.investree.demo.model;

import lombok.Data;

import java.io.Serializable;

import jakarta.persistence.*;

@Data
@Entity
@Table(name = "user_detail")
public class UserDetail implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_user", referencedColumnName = "id")
    private Users user;

    @Column(name = "nama", nullable = false, length = 64)
    private String nama;

    @Column(name = "alamat", length = 100)
    private String alamat;
}
