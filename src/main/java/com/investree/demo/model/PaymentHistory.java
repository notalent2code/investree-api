package com.investree.demo.model;

import lombok.Data;

import jakarta.persistence.*;

@Data
@Entity
@Table(name = "payment_history")
public class PaymentHistory {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "id_transaksi", referencedColumnName = "id")
    private Transaksi transaksi;
    
    @Column(name = "pembayaran_ke")
    private int pembayaranKe;

    @Column(name = "jumlah")
    private double jumlah;

    @Column(name = "bukti_pembayaran")
    private String buktiPembayaran;

}