package com.investree.demo.model;

import java.io.Serializable;

import jakarta.persistence.*;

import lombok.Data;

@Data
@Entity
@Table(name = "transaksi")
public class Transaksi implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "id_peminjam", referencedColumnName = "id")
    private Users peminjam;

    @ManyToOne
    @JoinColumn(name = "id_meminjam", referencedColumnName = "id")
    private Users meminjam;

    @Column(name = "tenor", nullable = false)
    private int tenor;

    @Column(name = "total_pinjaman", nullable = false)
    private double totalPinjaman;

    @Column(name = "bunga_persen", nullable = false)
    private double bungaPersen;

    @Column(name = "status")
    private String status;

}
