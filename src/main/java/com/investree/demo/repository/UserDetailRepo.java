package com.investree.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.investree.demo.model.UserDetail;

public interface UserDetailRepo extends JpaRepository<UserDetail, Long>{
    @Query("SELECT u FROM UserDetail u")
    public List<UserDetail> findAllUserDetail();

    @Query("SELECT u FROM UserDetail u WHERE u.id = :id")
    public Optional<UserDetail> findById(@Param("id") Long id);

}
