package com.investree.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.investree.demo.model.PaymentHistory;

@Repository
public interface PaymentHistoryRepo extends JpaRepository<PaymentHistory, Long>{
    @Query("SELECT p FROM PaymentHistory p ")
    public Iterable<PaymentHistory> findAllPaymentHistory();

    @Query("SELECT p FROM PaymentHistory p WHERE p.id = :id")
    public Optional<PaymentHistory> findById(@Param("id") Long id);
}
