package com.investree.demo.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.investree.demo.model.Transaksi;

@Repository
public interface TransaksiRepo extends JpaRepository<Transaksi, Long> {
    @Query("SELECT t FROM Transaksi t ")
    public Page<Transaksi> findAllTransaksi(Pageable pageable);

    @Query("SELECT t FROM Transaksi t WHERE t.id = :id")
    public Optional<Transaksi> findById(@Param("id") Long id);
      
    @Query("SELECT t FROM Transaksi t WHERE t.status LIKE %:status%")
    public Page<Transaksi> findByStatusLike(@Param("status") String status, Pageable pageable);

}
