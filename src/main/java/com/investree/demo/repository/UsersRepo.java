package com.investree.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.investree.demo.model.Users;

@Repository
public interface UsersRepo extends JpaRepository<Users, Long> {
    @Query("SELECT u FROM Users u")
    public List<Users> findAllUsers();

    @Query("SELECT u FROM Users u WHERE u.id = :id")
    public Optional<Users> findById(@Param("id") Long id);

    @Query("SELECT u FROM Users u WHERE u.userName = :userName")
    public Users findByUserName(@Param("userName") String userName);
}