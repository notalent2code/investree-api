package com.investree.demo.view.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import jakarta.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.investree.demo.model.Transaksi;
import com.investree.demo.repository.TransaksiRepo;
import com.investree.demo.view.TransaksiService;

@Service
@Transactional
public class TransaksiPaymentImple implements TransaksiService {
    @Autowired
    public TransaksiRepo repo;

    @Override
    public Map<Object, Object> save(Transaksi transaksi) {
        Map<Object, Object> map = new HashMap<>();
        
        try {
            Transaksi trx = repo.save(transaksi);

            map.put("data", trx);
            map.put("statusCode", "200");
            map.put("statusMessage", "Data berhasil disimpan");
            
            return map;

        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", "Data gagal disimpan");
            
            return map;
        }
    }

    @Override
    public Map<Object, Object> updateStatus(Transaksi transaksi) {
        Map<Object, Object> map = new HashMap<>();

        try {
            Optional<Transaksi> trx = repo.findById(transaksi.getId());

            Transaksi obj = repo.findById(transaksi.getId()).get();

            if (trx == null) {
                map.put("statusCode", "404");
                map.put("statusMessage", "Data tidak ditemukan");
                return map;
            }

            obj.setStatus("Lunas");
            repo.save(obj);

            map.put("data", trx);
            map.put("statusCode", "200");
            map.put("statusMessage", "Data berhasil diupdate");
            
            return map;

        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", "Data gagal diupdate");
            
            return map;
        }
    }
}
