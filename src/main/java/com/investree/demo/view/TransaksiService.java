package com.investree.demo.view;

import java.util.Map;

import com.investree.demo.model.Transaksi;

public interface TransaksiService {
    public Map<Object, Object> save(Transaksi transaksi);

    public Map<Object, Object> updateStatus(Transaksi transaksi);

}
