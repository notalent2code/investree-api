package com.investree.demo.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.investree.demo.model.Transaksi;
import com.investree.demo.repository.TransaksiRepo;
import com.investree.demo.view.TransaksiService;

@RestController
@RequestMapping("/v1/transaksi")
public class TransaksiController {
    @Autowired
    public TransaksiRepo repo;

    @Autowired
    TransaksiService service;

    @GetMapping("/{id}")
    public ResponseEntity<Transaksi> findById(@PathVariable Long id) {
        return ResponseEntity.ok(repo.findById(id).get());
    }

    @GetMapping("/list")
    public ResponseEntity<Page<Transaksi>> list(
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "10") Integer size,
            @RequestParam(required = false) String status) {
        Pageable paging = PageRequest.of(page, size);
        if (status == null || status.isEmpty()) {
            Page<Transaksi> list = repo.findAllTransaksi(paging);
            return new ResponseEntity<Page<Transaksi>>(list, new HttpHeaders(), HttpStatus.OK);
        }
        Page<Transaksi> list = repo.findByStatusLike("%" + status + "%", paging);
        return new ResponseEntity<Page<Transaksi>>(list, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Map<Object, Object>> save(Transaksi objModel) {
        return ResponseEntity.ok(service.save(objModel));
    }

    @PutMapping
    public ResponseEntity<Map<Object, Object>> update(Transaksi objModel) {
        return ResponseEntity.ok(service.updateStatus(objModel));
    }


}
