package com.investree.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.investree.demo.model.PaymentHistory;
import com.investree.demo.repository.PaymentHistoryRepo;

@RestController
@RequestMapping("/v1/paymentHistory")
public class PaymentHistoryController {
    @Autowired
    public PaymentHistoryRepo repo;

    @GetMapping
    public Iterable<PaymentHistory> list() {
        Iterable<PaymentHistory> list = repo.findAllPaymentHistory();
        return list;
    }

}
