package com.investree.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.investree.demo.model.Users;
import com.investree.demo.repository.UsersRepo;

@RestController
@RequestMapping("/v1/users")
public class UsersController {
    @Autowired
    public UsersRepo repo;

    @GetMapping
    public List<Users> list() {
        List<Users> map = repo.findAllUsers();
        return map;
    }

}
