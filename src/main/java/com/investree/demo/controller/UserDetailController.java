package com.investree.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.investree.demo.model.UserDetail;
import com.investree.demo.repository.UserDetailRepo;

@RestController
@RequestMapping("/v1/userDetail")
public class UserDetailController {
    @Autowired
    public UserDetailRepo repo;

    @GetMapping
    public List<UserDetail> list() {
        List<UserDetail> list = repo.findAllUserDetail();
        return list;
    }

}
